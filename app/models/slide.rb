class Slide < ApplicationRecord

validates :name, presence: true
has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/

scope :publication, -> { where(published: true) }
scope :min_publication, -> { where('DateTime(published_from) < ? ', DateTime.now ) }
scope :max_publication, -> { where('DateTime(published_to) > ?', DateTime.now ) }

end
