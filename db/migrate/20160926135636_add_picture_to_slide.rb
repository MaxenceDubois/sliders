class AddPictureToSlide < ActiveRecord::Migration[5.0]
  def change
  	add_attachment :slides, :picture
  end
end
